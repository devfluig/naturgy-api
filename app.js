import express from 'express';
import routes from './src/routes/index.js';
const app = express();



app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use("/", routes);


app.get("/", (req, res) => {
    res.status(200).json({
        "NAME": "Naturgy API prod v.0.0",
        "DESCRIPTION": "API para fazer a conexão entre Blip e API da Naturgy",
        "VERSION": "v.0.0",
        "ENVIRONMENT": "Prod"
    })
})


export default app;