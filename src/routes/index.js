'use strict';
import express from 'express';


import {
    getClient, getDebts, requestInstallment, consultCutSupply,
    recoverPersonalData, consultInstallment, requestRebindingByTrust,
    retriveOutstandingInvoices, consultInvoicesByCPF, getContractsByCPF, 
    getLastInvoiceIssued, consultPoint, getPixCode, getAllPixCodes
} from '../controllers/index.js';

const router = express.Router();



router.post("/get-client", getClient)
router.post("/get-debts", getDebts)
router.post("/request-installment", requestInstallment);
router.post("/consult-cut-supply", consultCutSupply);
router.post("/recover-personal-data", recoverPersonalData);
router.post("/consult-installment", consultInstallment);
router.post("/request-rebinding-by-trust", requestRebindingByTrust);
router.post("/retrive-outstanding-invoices", retriveOutstandingInvoices);
router.post("/consult-invoices-by-cpf", consultInvoicesByCPF);
router.post("/get-contracts-by-cpf", getContractsByCPF);
router.post("/get-last-invoice-issued", getLastInvoiceIssued);
router.post("/consult-point", consultPoint);
router.post("/get-pix-code", getPixCode);
router.post("/get-all-pix-code", getAllPixCodes);




export default router;