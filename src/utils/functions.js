export async function getLastInvoiceIssuedFunction(invoices) {
  try {
    let lastInvoiceIssued = null;

    for (let invoice of invoices) {

      if (!lastInvoiceIssued && invoice['DATA_EMISSAO'] && invoice['STATUS'] == 'Paga') {
        lastInvoiceIssued = invoice['URL'];
        continue;
      }

      if (invoice['DATA_EMISSAO'] > lastInvoiceIssued && invoice['STATUS'] == 'Paga') {
        lastInvoiceIssued = invoice['URL'];
      }
    }

    return lastInvoiceIssued;
  } catch (error) {
    console.log(error);
    return null;
  }

}