import axios from 'axios';
import dotenv from 'dotenv';

import qs from 'qs';

import { getLastInvoiceIssuedFunction } from '../utils/functions.js';

dotenv.config();


const headerTemplate = (authorization) => {
    return {
        "Authorization": authorization,
        "Content-Type": "application/x-www-form-urlencoded"
    }
}

const getClient = async (req, res) => {

    const { cpf_cnpj } = req.body;

    const { authorization } = req.headers;


    const URL = 'https://servicosnaturgy.ceg.com.br/zenbiapiprd/biaiapi/Consultas?consulta=ConsultarCliente';
    const options = {
        method: 'POST',
        headers: headerTemplate(authorization),
        data: qs.stringify({
            cpf_cnpj: cpf_cnpj
        }),
        url: URL

    }
    try {
        let client = await axios(options)
        client = client.data;

        if (!client) {
            throw new Error("Cannot possible find the client")
        }

        if (Object.values(client).length === 0) {
            return res.status(202).send({
                message: "There is no customer with this cpf/cnpj",
                hasClient: false,
                data: null
            })
        }


        return res.status(200).send({
            message: "The client was found successfully",
            hasClient: true,
            data: client
        })
    }
    catch (error) {
        console.log(error);
        return res.status(error.status || 400).send({
            message: "An error occurred while getting the client",
            error: error

        })
    }
}

const getChecker= async (authorization, empresa, num_cliente, cups) => {
    const URL = 'https://servicosnaturgy.ceg.com.br/zenbiapiprd/biaiapi/Consultas?consulta=RecuperarDadosPessoaisCliente';

    const options = {
        method: 'POST',
        headers: headerTemplate(authorization),
        data: qs.stringify({
            empresa: empresa,
            numero_cliente: num_cliente,
            cups: cups,
        }),
        url: URL
    }


    let personalData = await axios(options);
    personalData = personalData.data;


    if (!personalData) throw new Error("No personal data available")

    return personalData;
}

const getDatesDebts = async (authorization, cpf_cnpj, poliza_key) => {
    const URL = 'https://servicosnaturgy.ceg.com.br/zenbiapiprd/biaiapi/Consultas?consulta=ConsultarDividaPorCpf';
    const options = {
        method: 'POST',
        headers: headerTemplate(authorization),
        data: qs.stringify({
            cpf_cnpj: cpf_cnpj,
            poliza_key: poliza_key
        }),
        url: URL
    }


    let debt = await axios(options);
    debt = debt?.data;

    return debt;
}

const getPix = async (token, empresa, num_cliente, digito, cpf_cnpj, mes, ano) => {
    const URL = "https://fol.gasnaturalbrasil.com.br/FatemailWeb/FaturaApi/PixJson"

    const options = {
        method: 'POST',
        data: {
            "Token": token,
            "Empresa": empresa,
            "Cliente": num_cliente,
            "Digito": digito,
            "Documento": cpf_cnpj,
            "Mes": mes,
            "Ano": ano
        },
        url: URL
    }


    let pixCode = await axios(options);
    pixCode = pixCode.data;

    return pixCode;
}

const setDebtAmount = (debts) => {
    let total = 0.0;


    Object.values(debts).forEach((debt) => {
        total += debt.VALOR;
    })

    return total;

}

const defineInstallmentPossibilities = (totalDebt) => {
    let possibilities = {
        "3x": 0,
        "6x": 0,
        "12x": 0,
    };

    try {
        totalDebt = Number(totalDebt);

        //Preciso arredondar de forma precisa.

        possibilities['3x'] = (totalDebt / 3);
        possibilities['6x'] = (totalDebt / 6);
        possibilities['12x'] = (totalDebt / 12);


        return possibilities;
    }
    catch (error) {
        return null;
    }
}

const checkIfHasInstallmentProposal = (debts) => {
    let proposal = null;

    //existe proposta?
    proposal = Object.values(debts).filter(prop => prop.POSSUI_PROPOSTA !== "N");

    if (proposal && proposal.length === 0) {
        return false;
    }


    return true;
}

const getPolizaKey = (debts) => {
    let poliza = null;

    let array = Object.values(debts);

    for (let i = 0; i < array.length; i++) {
        if (array[i].POLIZA_KEY) {
            poliza = array[i].POLIZA_KEY;
            break;
        }
    }

    return poliza;
}

const getDebts = async (req, res) => {
    const { cpf_cnpj, poliza_key } = req.body;

    const { authorization } = req.headers;


    const URL = 'https://servicosnaturgy.ceg.com.br/zenbiapiprd/biaiapi/Consultas?consulta=ConsultarDividaPorCpf';
    const options = {
        method: 'POST',
        headers: headerTemplate(authorization),
        data: qs.stringify({
            cpf_cnpj: cpf_cnpj,
            poliza_key: poliza_key
        }),
        url: URL

    }
    try {
        let debt = await axios(options);

        debt = debt?.data;
        if (!debt) {
            throw new Error("Cannot possible find debts")
        }

        if (Object.values(debt).length === 0) {
            return res.status(202).send({
                message: "There are no debts linked to this cpf/cnpj",
                hasDebts: false,
                data: null
            })
        }

        const totalDebt = setDebtAmount(debt);
        const installmentProposal = checkIfHasInstallmentProposal(debt);
        const installmentPossibilities = defineInstallmentPossibilities(totalDebt);
        const polizaKey = getPolizaKey(debt);

        return res.status(200).send({
            message: "debts linked to your cpf/cnpj were found",
            hasDebts: true,
            cpf_cnpj: cpf_cnpj,
            poliza_key: polizaKey,
            installmentProposal: installmentProposal,
            installmentPossibilities: installmentPossibilities,
            totalDebt: totalDebt,
            data: debt
        })
    }
    catch (error) {
        console.log(error);
        return res.status(error.status || 400).send({
            message: "An error occurred while getting the debts",
            error: error

        })
    }

}


const requestInstallment = async (req, res) => {

    const {
        cpf_cnpj, sistema_origem, protocolo,
        poliza_key, qtd_parcelas, valor_p1, valor_px,
        nome_cliente, email_cliente, proposta_acordo_nr_sequencial
    } = req.body;


    const { authorization } = req.headers;

    const URL = 'https://servicosnaturgy.ceg.com.br/zenbiapiprd/biaiapi/Consultas?consulta=SolicitarParcelamento';
    const options = {
        method: 'POST',
        headers: headerTemplate(authorization),
        data: qs.stringify({
            cpf_cnpj: cpf_cnpj,
            poliza_key: poliza_key,
            sistema_origem: sistema_origem,
            protocolo: protocolo,
            qtd_parcelas: qtd_parcelas,
            valor_p1: valor_p1,
            valor_px: valor_px,
            nome_cliente: nome_cliente,
            email_cliente: email_cliente,
            proposta_acordo_nr_sequencial: proposta_acordo_nr_sequencial,
        }),
        url: URL

    }
    try {
        let installment = await axios(options);
        installment = installment?.data;

        if (!installment) {
            throw new Error("Cannot possible request the installment")
        }


        return res.status(200).send({
            message: "Your payment request has been successfully completed.",
            data: installment
        })
    }
    catch (error) {
        console.log(error);
        return res.status(error.status || 400).send({
            message: "An error occurred while requesting payment in installments",
            error: error

        })

    }
}



const consultCutSupply = async (req, res) => {
    const { cpf_cnpj, poliza_key } = req.body;

    const { authorization } = req.headers;


    const URL = 'https://servicosnaturgy.ceg.com.br/zenbiapiprd/biaiapi/Consultas?consulta=ConsultarFornecimentoCortado';

    const options = {
        method: 'POST',
        headers: headerTemplate(authorization),
        data: qs.stringify({
            cpf_cnpj: cpf_cnpj,
            poliza_key: poliza_key
        }),
        url: URL
    }
    try {
        let supply = await axios(options);
        supply = supply?.data;

        if (!supply) throw new Error("asmj")


        return res.status(200).send({
            message: "",
            data: supply
        })
    }
    catch (error) {
        console.log(error);
        return res.status(error.status || 400).send({
            message: "An error occurred while requesting payment in installments",
            error: error
        })
    }
}


const recoverPersonalData = async (req, res) => {

    const { empresa, numero_cliente, cups } = req.body;

    const { authorization } = req.headers;

    const URL = 'https://servicosnaturgy.ceg.com.br/zenbiapiprd/biaiapi/Consultas?consulta=RecuperarDadosPessoaisCliente';


    const options = {
        method: 'POST',
        headers: headerTemplate(authorization),
        data: qs.stringify({
            empresa: empresa,
            numero_cliente: numero_cliente,
            cups: cups,
        }),
        url: URL
    }


    try {
        let personalData = await axios(options);
        personalData = personalData.data;


        if (!personalData) throw new Error("No personal data available")


        return res.status(200).send({
            message: "your personal data was found",
            data: personalData
        })
    }
    catch (error) {
        console.log(error);
        return res.status(error.status || 400).send({
            message: "an error occurred while fetching your personal data",
            error: error
        })
    }
}

const consultInstallment = async (req, res) => {

    const { cpf_cnpj, poliza_key } = req.body;
    const { authorization } = req.headers;

    const URL = 'https://servicosnaturgy.ceg.com.br/zenbiapiprd/biaiapi/Consultas?consulta=ConsultarParcelamento';


    const options = {
        method: 'POST',
        headers: headerTemplate(authorization),
        data: qs.stringify({
            cpf_cnpj: cpf_cnpj,
            poliza_key: poliza_key
        }),
        url: URL
    }

    try {
        let installments = await axios(options);
        installments = installments?.data;


        if (!installments) throw new Error("")

        if (Object.values(installments).length === 0) {
            return res.status(202).send({
                message: "",
                hasInstallments: false,
                data: null
            })
        }


        return res.status(200).send({
            message: "",
            hasInstallments: true,
            data: installments
        })
    }
    catch (error) {
        console.log(error);
        return res.status(error.status || 400).send({
            message: "an error occurred while your installments",
            error: error
        })
    }
}

const requestRebindingByTrust = async (req, res) => {
    const { cpf_cnpj, poliza_key, nome_cliente,
        sistema_origem, protocolo, email_cliente } = req.body;
    const { authorization } = req.headers;

    const URL = 'https://servicosnaturgy.ceg.com.br/zenbiapiprd/biaiapi/Consultas?consulta=SolicitarReligacaoConfianca';


    const options = {
        method: 'POST',
        headers: headerTemplate(authorization),
        data: qs.stringify({
            cpf_cnpj: cpf_cnpj,
            poliza_key: poliza_key,
            nome_cliente: nome_cliente,
            email_cliente: email_cliente,
            sistema_origem: sistema_origem,
            protocolo: protocolo,
        }),
        url: URL
    }

    try {
        let rebinding = await axios(options);

        rebinding = rebinding?.data;

        if (!rebinding) throw new Error("An error occurred while processing your request")


        return res.status(200).send({
            message: "Your trust rebind request was successful",
            data: rebinding
        })

    }
    catch (error) {
        console.log(error);
        return res.status(error.status || 400).send({
            message: "An error occurred while processing your request",
            error: error
        })

    }
}

const retriveOutstandingInvoices = async (req, res) => {
    const { empresa, numero_cliente, data_vencimento_inicio, data_vencimento_fim } = req.body;
    const { authorization } = req.headers;

    const URL = 'https://servicosnaturgy.ceg.com.br/zenbiapiprd/biaiapi/Consultas?consulta=RecuperarFaturasPendentesPagamento';


    const options = {
        method: 'POST',
        headers: headerTemplate(authorization),
        data: qs.stringify({
            empresa: empresa,
            numero_cliente: numero_cliente,
            data_vencimento_inicio: data_vencimento_inicio,
            data_vencimento_fim: data_vencimento_fim,
        }),
        url: URL
    }


    try {
        let outstandingInvoices = await axios(options);
        outstandingInvoices = outstandingInvoices?.data;


        if (!outstandingInvoices) throw new Error('');

        if (Object.values(outstandingInvoices).length === 0) {
            return res.status(202).send({
                message: "No outstanding invoices were found",
                hasOutstandingInvoices: false,
                data: null
            })
        }

        return res.status(200).send({
            message: "These outstanding invoices linked to your data were found",
            hasOutstandingInvoices: true,
            data: outstandingInvoices
        })

    }
    catch (error) {
        console.log(error);
        return res.status(error.status || 400).send({
            message: "An error occurred while processing your request",
            error: error
        })
    }
}

const consultInvoicesByCPF = async (req, res) => {

    const { cpf_cnpj } = req.body;
    const { authorization } = req.headers;

    const URL = 'https://servicosnaturgy.ceg.com.br/zenbiapiprd/biaiapi/Consultas?consulta=ConsultarFaturasPorCPF';


    const options = {
        method: 'POST',
        headers: headerTemplate(authorization),
        data: qs.stringify({
            cpf_cnpj: cpf_cnpj,
        }),
        url: URL
    }


    try {
        let invoices = await axios(options);
        invoices = invoices?.data;

        if (!invoices) throw new Error("")

        if (Object.values(invoices).length === 0) {
            return res.status(202).send({
                message: "No invoices were found",
                hasInvoices: false,
                data: null
            })
        }


        return res.status(200).send({
            message: "The following invoices linked to your data were found",
            hasInvoices: true,
            data: invoices
        })
    }
    catch (error) {
        console.log(error);
        return res.status(error.status || 400).send({
            message: "An error occurred while processing your request",
            error: error
        })
    }
}

const getLastInvoiceIssued = async (req, res) => {

    const { cpf_cnpj } = req.body;
    const { authorization } = req.headers;

    const URL = 'https://servicosnaturgy.ceg.com.br/zenbiapiprd/biaiapi/Consultas?consulta=ConsultarFaturasPorCPF';


    const options = {
        method: 'POST',
        headers: headerTemplate(authorization),
        data: qs.stringify({
            cpf_cnpj: cpf_cnpj,
        }),
        url: URL
    }

    try {
        let invoices = await axios(options);
        invoices = invoices?.data;

        if (!invoices) throw new Error("")

        if (Object.values(invoices).length === 0) {
            return res.status(202).send({
                message: "No invoices were found",
                hasInvoice: false,
                data: null
            })
        }

        let lastInvoiceIssued = await getLastInvoiceIssuedFunction(invoices);

        if (!lastInvoiceIssued) {
            return res.status(202).send({
                message: "The last invoice issued was not found",
                hasInvoice: false,
                data: null
            })
        }

        return res.status(200).send({
            message: "The last invoice issued was found",
            hasInvoice: true,
            data: lastInvoiceIssued
        })
    }
    catch (error) {
        console.log(error);
        return res.status(error.status || 400).send({
            message: "An error occurred while processing your request",
            error: error
        })
    }
}

const getContractsByCPF = async (req, res) => {
    const { cpf_cnpj } = req.body;
    const { authorization } = req.headers;

    const URL = 'https://servicosnaturgy.ceg.com.br/zenbiapiprd/biaiapi/Consultas?consulta=ConsultarFaturasPorCPF';

    const options = {
        method: 'POST',
        headers: headerTemplate(authorization),
        data: qs.stringify({
            cpf_cnpj: cpf_cnpj,
        }),
        url: URL
    }

    try {
        let invoices = await axios(options);
        invoices = invoices?.data;

        if (!invoices) throw new Error("")

        if (Object.values(invoices).length === 0) {
            return res.status(202).send({
                message: "It was not possible to associate any contracts with your data",
                hasContracts: false,
                numberOfContracts: 0,
                data: null
            })
        }

        //Contracts are identified by the keys "poliza_key"
        const uniquePolizas = [... new Map(
            invoices.map(item => [item["POLIZA_KEY"], item])
        ).values()];

        if (!uniquePolizas) throw new Error("")

        return res.status(200).send({
            message: "Found one or more contracts associated with your data",
            hasContracts: true,
            numberOfContracts: uniquePolizas.length,
            data: uniquePolizas
        })


    }
    catch (error) {
        console.log(error);
        return res.status(error.status || 400).send({
            message: "An error occurred while processing your request",
            error: error
        })
    }
}

const consultPoint = async (req, res) => {

    const { empresa, numero_cliente, cpf_cnpj, 
            endereco, cep, numero_endereco, 
            bairro, municipio, codigo_endereco, 
            cups, situacao } = req.body;
    const { authorization } = req.headers;

    const URL = 'https://servicosnaturgy.ceg.com.br/zenbiapiprd/biaiapi/Consultas?consulta=ConsultarPonto';


    const options = {
        method: 'POST',
        headers: headerTemplate(authorization),
        data: qs.stringify({
            empresa: empresa,
            numero_cliente: numero_cliente,
            cpf_cnpj: cpf_cnpj,
            endereco: endereco,
            cep: cep,
            numero_endereco: numero_endereco,
            bairro: bairro,
            municipio: municipio,
            codigo_endereco: codigo_endereco,
            cups: cups,
            situacao: situacao
        }),
        url: URL
    }


    try {
        let point = await axios(options);
        point = point?.data;

        if (!point) throw new Error("")

        if (Object.values(point).length === 0) {
            return res.status(202).send({
                message: "No point were found",
                hasPoint: false,
                data: null
            })
        }


        return res.status(200).send({
            message: "The following point linked to your data were found",
            hasPoint: true,
            data: point
        })
    }
    catch (error) {
        console.log(error);
        return res.status(error.status || 400).send({
            message: "An error occurred while processing your request",
            error: error
        })
    }
}

const getPixCode = async (req, res) => {
    try {
        const { Token, Empresa, Cliente, Digito, Documento, Mes, Ano } = req.body;

        const URL = "https://fol.gasnaturalbrasil.com.br/FatemailWeb/FaturaApi/PixJson";

        const responseAxios = await axios({
            method: 'POST',
            data: {
                "Token": Token,
                "Empresa": Empresa,
                "Cliente": Cliente,
                "Digito": Digito,
                "Documento": Documento,
                "Mes": Mes,
                "Ano": Ano
            },
            url: URL
        })

        const response = responseAxios.data
        const responseFilter = response.QrCodeTexto

        return res.status(200).send({
            QrCodeTexto: responseFilter
        })
    }
    catch (error) {
        console.log(error)
        return res.status(error.status || 400).send({
            message: "Uma exceção aconteceu enquanto buscava os agendamentos",
            data: error
        });
    }
}

const getAllPixCodes = async (req, res) => {
    try {
        const { Token, Empresa, Cliente, Documento, Cups, Poliza_Key } = req.body;
        const { authorization } = req.headers;

        let response;
        let issueDate;
        let dueDate;
        let company;
        let cpfCnpj;
        let dates = [];
        let responseData = [];
        

        if (Empresa == "1") {
            company = "CEG";
        }
        if (Empresa == "2") {
            company = "CEG - RIO";
        }
        if (Empresa == "3") {
            company = "GNSPS";
        }

        let checker = await getChecker(authorization, company, Cliente, Cups);

        checker = checker[0].DV;

        let dateDebts = await getDatesDebts(authorization, Documento, Poliza_Key);
        
        if (Documento.length == 11) {
			cpfCnpj = "000" + Documento;
		}
        else {
            cpfCnpj = Documento
        }

        Object.entries(dateDebts).forEach(async ([key, value]) => {
            if (typeof value === 'object' && !Array.isArray(value) && value !== null) {
                issueDate = value.DATA_EMISSAO;
                dueDate = value.DATA_VENCIMENTO;
                let date = issueDate.split("-");
                
                dates.push({
                    "mes": date[1], 
                    "ano": date[0],
                    "vencimento": dueDate
                });

            }
            else {
                dates = [];
            }
        })

        for (let i = 0; i < dates.length; i++) {
            response = await getPix(Token, Empresa, Cliente, checker, cpfCnpj, dates[i].mes, dates[i].ano);

            if (response.QrCodeTexto != null) {
                responseData.push({
                    "data_emissao": issueDate, 
                    "data_vencimento": dates[i].vencimento, 
                    "pix_code": response.QrCodeTexto
                });
            }
        }

        if (responseData.length > 0) {
            return res.status(200).send({
                message: "Pix codes found",
                hasPix: true,
                data: responseData
            })
        }
        else {
            return res.status(200).send({
                message: "Pix codes not found",
                hasPix: false,
                data: null
            })
        }   
    }
    catch (error) {
        console.log(error)
        return res.status(error.status || 400).send({
            message: "An error occurred while processing your request",
            error: error
        })
    }
}

export {
    getClient, getDebts, requestInstallment, consultCutSupply,
    recoverPersonalData, consultInstallment, requestRebindingByTrust,
    retriveOutstandingInvoices, consultInvoicesByCPF, getContractsByCPF, 
    getLastInvoiceIssued, consultPoint, getPixCode, getAllPixCodes
}